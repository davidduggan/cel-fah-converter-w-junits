
public class CelsiusAndFahrenheit 
{
	public static double CelsiusToFahrenheit(double Celsius)
	{
		if(Celsius >= 0 && Celsius <= 100)
		{
			return (Celsius * 9/5) + 32;
		}
		else 
		{
			return 999;
		}
	}
	
	public static double FahrenheitToCelsius(double Fahrenheit)
	{ 
		if(Fahrenheit >= 0 && Fahrenheit <= 212)
		{
			return (Fahrenheit - 32) * 5/9;
		}
		else 
		{
			return 999;
		}
	}

	public static void main(String[] args) 
	{
		System.out.println(FahrenheitToCelsius());
	}

}
