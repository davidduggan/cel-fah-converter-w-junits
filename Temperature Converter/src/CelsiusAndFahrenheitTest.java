import junit.framework.TestCase;

public class CelsiusAndFahrenheitTest extends TestCase 
{
	//CelsiusToFahrenheit Method
	
	//EP
	
	//Test #: 1
	//Test Objective: Numbers 0 - 100
	//Test Input(s): 55
	//Test Expected Output(s): 131 
	
	public void  testCelsiusToFahrenheit001()
	{
		assertEquals(131.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(55));
	}
	
	//Test #: 2
	//Test Objective: Negative Numbers
	//Test Input(s): -12
	//Test Expected Output(s): 999
	
	public void  testCelsiusToFahrenheit002()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(-12));
	}
	
	//Test #: 3
	//Test Objective: Numbers above 100
	//Test Input(s): 154
	//Test Expected Output(s): 999
	
	public void  testCelsiusToFahrenheit003()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(154));
	}

	//Test #: 4
	//Test Objective: Zero
	//Test Input(s): 0
	//Test Expected Output(s): 32
	
	public void  testCelsiusToFahrenheit004()
	{
		assertEquals(32.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(0));
	}
	
	//Test #: 5
	//Test Objective: Invalid Values
	//Test Input(s): 562
	//Test Expected Output(s): 999
	
	public void  testCelsiusToFahrenheit005()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(562));
	}

	//BVA
	
	//Test #: 6
	//Test Objective: MINDouble
	//Test Input(s): Double.MIN_VALUE
	//Test Expected Output(s): 999 
	
	public void  testCelsiusToFahrenheit006()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(-Double.MIN_VALUE));
	}
	
	//Test #: 7
	//Test Objective: MAXDouble
	//Test Input(s): Double.MAX_VALUE
	//Test Expected Output(s): 999 
	
	public void  testCelsiusToFahrenheit007()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(Double.MAX_VALUE));
	}
	
	//Test #: 8
	//Test Objective: -1
	//Test Input(s): -1
	//Test Expected Output(s): 999 
	
	public void  testCelsiusToFahrenheit008()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(-1));
	}
	
	//Test #: 9
	//Test Objective: 101
	//Test Input(s): 101
	//Test Expected Output(s): 999
	
	public void  testCelsiusToFahrenheit009()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.CelsiusToFahrenheit(101));
	}
	
	
	
	//FahrenheitToCelsius Method
	
	//EP
	
	//Test #: 10
	//Test Objective: Numbers 0 - 212
	//Test Input(s): 123
	//Test Expected Output(s): 50.55555555555556
	
	public void  testFahrenheitToCelsius001()
	{
		assertEquals(50.55555555555556, CelsiusAndFahrenheit.FahrenheitToCelsius(123));
	}
	
	//Test #: 11
	//Test Objective: Negative Numbers
	//Test Input(s): -195
	//Test Expected Output(s): 999
	
	public void  testFahrenheitToCelsius002()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(-195));
	}
	
	//Test #: 12
	//Test Objective: Numbers above 212
	//Test Input(s): 458
	//Test Expected Output(s): 999
	
	public void  testFahrenheitToCelsius003()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(458));
	}
	
	//Test #: 13
	//Test Objective: Zero
	//Test Input(s): 0
	//Test Expected Output(s): -17.77777777777778
	
	public void  testFahrenheitToCelsius004()
	{
		assertEquals(-17.77777777777778, CelsiusAndFahrenheit.FahrenheitToCelsius(0));
	}
	
	//Test #: 14
	//Test Objective: Invalid Values
	//Test Input(s): 864
	//Test Expected Output(s): 999
	
	public void  testFahrenheitToCelsius005()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(864));
	}
	
	//BVA
	
	//Test #: 15
	//Test Objective: MINDouble
	//Test Input(s): Double.MIN_VALUE
	//Test Expected Output(s): 999 
	
	public void  testFahrenheitToCelsius006()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(-Double.MIN_VALUE));
	}

	//Test #: 16
	//Test Objective: MAXDouble
	//Test Input(s): Double.MAX_VALUE
	//Test Expected Output(s): 999 
	
	public void  testFahrenheitToCelsius007()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(Double.MAX_VALUE));
	}

	//Test #: 17
	//Test Objective: -1
	//Test Input(s): -1
	//Test Expected Output(s): 999 
	
	public void testFahrenheitToCelsius008()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(-1));
	}

	//Test #: 18
	//Test Objective: 213
	//Test Input(s): 213
	//Test Expected Output(s): 999
	
	public void  testFahrenheitToCelsius009()
	{
		assertEquals(999.0, CelsiusAndFahrenheit.FahrenheitToCelsius(213));
	}
	
}
